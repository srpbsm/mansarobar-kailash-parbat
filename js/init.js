;(function ($) {
  $(function () {
    $('.sidenav').sidenav()

    //init carousel
    $(document).ready(function () {
      $('.carousel').carousel({
        // dist: 0,
        // padding: 0,
        fullWidth: true,
        autoplay: true,
        indicators: true,
        duration: 500,
      })

      autoplay()
      function autoplay() {
        $('.carousel').carousel('next')
        setTimeout(autoplay, 4500)
      }
    })

    // $(document).ready(function () {
    //   $('.parallax').parallax()
    // })

    $(document).ready(function () {
      $('.collapsible').collapsible()
    })
  }) // end of document ready
})(jQuery) // end of jQuery name space
